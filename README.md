# groovymarker

Freemarker to groovy transpiler that may translate any freemarker template that solely consists of
directive calls (currently: [#assign], [#if], [#else if], [#else], [#list]) and interpolations
(e.g. `${expression}`) and does not contain any raw text.

The `transpile-template` subcommand can be used to transpile a given template and print the result.

The `migrate-table` subcommand can be used to transpile all freemarker templates in the given column
of the given table and generated a changeset for each successfully migrated row and create a changelog
file in the current directory.

In addition to simply translating the template the transpiler also optimises suboptimal entity paths, this can be
disabled using the `-n`, `--no-optimise` option.

e.g.

```freemarker
[#assign event = singleBase.relLecturer_booking.relEvent]
[#assign reservation = singleBase.relReservation]
[#assign reservationType = singleBase.relReservation.relReservation_type.unique_id]
[#assign abbr = singleBase.relLecturer_booking.relEvent.abbreviation]
[#assign gender = singleBase.relLecturer_booking.relUser.relGender.unique_id]
[#assign type = singleBase.relLecturer_booking.relUser.relUser_type.unique_id]
```

becomes

```groovy
def relLecturer_bookingGenerated0 = singleBase.relLecturer_booking
def event = relLecturer_bookingGenerated0.relEvent
def reservation = singleBase.relReservation
def reservationType = reservation.relReservation_type.unique_id
def abbr = event.abbreviation
def relUserGenerated1 = relLecturer_bookingGenerated0.relUser
def gender = relUserGenerated1.relGender.unique_id
def type = relUserGenerated1.relUser_type.unique_id
```

However, this is only performed for assignments within the same scope.

If possible, this uses existing paths rather than generating a new assignment, reordering the existing assignment i
necessary. E.g.:

```freemarker
[#assign event = singleBase.relReservation_lecturer_booking.relLecturer_booking.relEvent.pk]
[#assign user = singleBase.relReservation_lecturer_booking.relLecturer_booking.relUser.pk]
[#assign booking = singleBase.relReservation_lecturer_booking.relLecturer_booking]
```

is transpiled to:

```groovy
def booking = singleBase.relReservation_lecturer_booking.relLecturer_booking
def event = booking.relEvent.pk
def user = booking.relUser.pk
```

If the path contains function calls it is only optimised up to the first function call, as function calls could have
side effects and thus should not be reduced to a common sub path. E.g.

```freemarker
[#assign fiveYearsFromNow = singleBase.relReservation.date_from.plusYears(5).toLocalDate()/]
[#assign tenYearsFromNow = singleBase.relReservation.date_from.plusYears(10).toLocalDate()/]
[#assign fourtyYearsFromNow = singleBase.relReservation.date_from.plusYears(40).toLocalDate()/]
[#assign fiftyfiveYearsFromNow = singleBase.relReservation.date_from.plusYears(55).toLocalDate()/]
```

transpiles to

```groovy
def date_fromGenerated0 = singleBase.relReservation.date_from
def fiveYearsFromNow = date_fromGenerated0.plusYears(5).toLocalDate()
def tenYearsFromNow = date_fromGenerated0.plusYears(10).toLocalDate()
def fourtyYearsFromNow = date_fromGenerated0.plusYears(40).toLocalDate()
def fiftyfiveYearsFromNow = date_fromGenerated0.plusYears(55).toLocalDate()
```

If the same path is repeated all subsequent assignments of the same path are simply assigned to the name of the first assignment, e.g.

```freemarker
[#assign event1 = singleBase.relLecturer_booking.relEvent]
[#assign event2 = singleBase.relLecturer_booking.relEvent]
[#assign event3 = singleBase.relLecturer_booking.relEvent]
[#assign booking = singleBase.relLecturer_booking]
```

transpiles to

```groovy
def booking = singleBase.relLecturer_booking
def event1 = booking.relEvent
def event2 = event1
def event3 = event1
```

## Bindings

When transpiling templates it's possible to specify bindings, remapping a source path to a target path, using the
`-b` / `--bindings` option. E.g., when using

```bash
groovymarker transpile-template -b singleBase.relLecturer_booking:booking,singleBase:reservationBooking "
[#assign event = singleBase.relLecturer_booking.relEvent]                                                
[#assign reservation = singleBase.relReservation]
[#assign reservationType = singleBase.relReservation.relReservation_type.unique_id]
[#assign abbr = singleBase.relLecturer_booking.relEvent.abbreviation]
[#assign gender = singleBase.relLecturer_booking.relUser.relGender.unique_id]
[#assign type = singleBase.relLecturer_booking.relUser.relUser_type.unique_id]
"
```

`singleBase.relLecturer_booking` is remapped to `booking` and `singleBase` is remapped to `reservationBooking`, producing
the following output:

```groovy
def event = booking.relEvent
def reservation = reservationBooking.relReservation
def reservationType = reservation.relReservation_type.unique_id
def abbr = event.abbreviation
def relUserGenerated0 = booking.relUser
def gender = relUserGenerated0.relGender.unique_id
def type = relUserGenerated0.relUser_type.unique_id
```

## Example

```freemarker
[#assign x = 7 arr = ['test1', 'test2]', '[test3'] foo]
[#assign y = 5]
[#if y > 4]
    [#assign y *= 3]
[#elseif y > 10]
    [#assign y /= 2]
[#else]
    [#assign y = 7]
[/#if]
[#assign is_even = x % 2 == 0 other = 1 + 2 * 6 other2 = 5 == (4 * 5 + 2) / 5 other3 = 2]
[#if x % 2 == 0]
    [#assign foo = 'even']
[#else]
    [#assign foo = 'uneven']
[/#if]
[#if arr?size > 2]
    [#list arr as el]
        [#if el == 'test1' || el == 'test2]' || '[test3']
            [#assign success = true]
        [#else]
            [#assign success = false]
        [/#if]
    [/#list]
[#else]
    [#assign success = false]
[/#if]
${(y == 7 && foo == 'uneven')?c}
```

is transpiled to

```groovy
def x = 7
def arr = ['test1', 'test2]', '[test3']
def foo
def y = 5
if (y > 4) {
    y *= 3
}
else if (y > 10) {
    y /= 2
}
else {
    y = 7
}
def is_even = x % 2 == 0
def other = 1 + 2 * 6
def other2 = 5 == (4 * 5 + 2) / 5
def other3 = 2
if (x % 2 == 0) {
    foo = 'even'
}
else {
    foo = 'uneven'
}
if (arr.size() > 2) {
    for (def el : arr) {
        if (el == 'test1' || el == 'test2]' || '[test3') {
            def success = true
        }
        else {
            def success = false
        }
    }
}
else {
    def success = false
}
(y == 7 && foo == 'uneven')
```

## Built-ins

Groovymarker supports certain freemarker built-ins, such as `?c`, `?size`, `?first`, `?split`, `?number`, `?contains`, `?length` and `?has_content`.
For example,

```freemarker
[#assign arr = [1, 2, 3]]
[#assign str = '']
[#assign number]
[#if arr?size >= 2]
  [#assign number = arr[0] + arr [1]]
[#else]
  [#if str?has_content]
    [#if !str?contains('.')]
      [#assign number = str?number]
    [#elseif str?split('.')?first == '6']
      [#assign number = str?number]
    [#elseif str?length > 1]
      [#assign number = 66]
    [/#if]
  [#else]
    [#assign number = 6]
  [/#if]
[/#if]
${number?c}
```

transpiles to

```groovy
def arr = [1, 2, 3]
def str = ''
def number
if (arr.size() >= 2) {
    number = arr[0] + arr [1]
}
else {
    if (str) {
        if (!str.contains('.')) {
            number = str.toDouble()
        }
        else if (str.split('.')[0] == '6') {
            number = str.toDouble()
        }
        else if (str.length() > 1) {
            number = 66
        }
    }
    else {
        number = 6
    }
}
number
```

## Migrate tables

Groovymarker may be used to generate changesets to migrate all rows of a table by specifying a column containing freemarker
templates that should be transpiled to groovy.

E.g. the command `groovymarker migrate-table -a rofr -v 2.27 -u ecap2test_user -p password -d nice2_ecap2test_rofr2 -t nice_wage_scale_modifier -f groovy_modifier -b singleBase:booking`
may produce the following changelog:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<databaseChangeLog xmlns="http://www.tocco.ch/xml/ns/dbchangelog" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.tocco.ch/xml/ns/dbchangelog http://www.tocco.ch/xml/ns/dbchangelog/dbchangelog-tocco-3.1.xsd">
  <changeSet author="rofr" dbms="postgresql" id="nice_wage_scale_modifier_ecap_groovy_migration-groovymarkergenid5TwF3Zu/2.27">
    <preConditions onFail="MARK_RAN">
      <sqlCheck expectedResult="1">select count(*) from nice_wage_scale_modifier where unique_id = 'ecap'</sqlCheck>
    </preConditions>
    <update tableName="nice_wage_scale_modifier">
      <column name="groovy_modifier" value="def relLecturer_bookingGenerated0 = booking.relLecturer_booking&#xA;def event = relLecturer_bookingGenerated0.relEvent&#xA;def user = relLecturer_bookingGenerated0.relUser&#xA;def factorFeiertag = 0.032&#xA;def holyday = 0&#xA;def education_amount = event.education_amount&#xA;def education_amount_unit = 0&#xA;def education_amount_seconds = 0&#xA;def education_amount_minutes = 0&#xA;if (education_amount &amp;&amp; education_amount != 0) {&#xA;    education_amount_seconds = education_amount / 1000&#xA;    education_amount_minutes = education_amount_seconds / 60&#xA;}&#xA;if (education_amount_minutes &gt; 0) {&#xA;    education_amount_unit = education_amount_minutes / 45&#xA;}&#xA;def lecturerDateOfBirth = user.birthdate&#xA;def lecturerEntryDate = user.entry_date&#xA;def date_fromGenerated1 = booking.relReservation.date_from&#xA;def fiveYearsFromNow = date_fromGenerated1.plusYears(-5).toLocalDate()&#xA;def tenYearsFromNow = date_fromGenerated1.plusYears(-10).toLocalDate()&#xA;def fourtyYearsFromNow = date_fromGenerated1.plusYears(-40).toLocalDate()&#xA;def fiftyfiveYearsFromNow = date_fromGenerated1.plusYears(-55).toLocalDate()&#xA;def lessThan40 = lecturerDateOfBirth &amp;&amp; !lecturerDateOfBirth.isBefore(fourtyYearsFromNow)&#xA;def between40and55 = lecturerDateOfBirth &amp;&amp; lecturerDateOfBirth.isBefore(fourtyYearsFromNow) &amp;&amp; !lecturerDateOfBirth.isBefore(fiftyfiveYearsFromNow)&#xA;def between5and10 = lecturerEntryDate &amp;&amp; lecturerEntryDate.isBefore(fiveYearsFromNow) &amp;&amp; !lecturerEntryDate.isBefore(tenYearsFromNow)&#xA;def moreThan55 = lecturerDateOfBirth &amp;&amp; lecturerDateOfBirth.isBefore(fiftyfiveYearsFromNow)&#xA;def level_one = lessThan40 &amp;&amp; lecturerEntryDate &amp;&amp; !lecturerEntryDate.isBefore(fiveYearsFromNow)&#xA;def level_two = lessThan40 &amp;&amp; between5and10&#xA;def level_three = lessThan40 &amp;&amp; lecturerEntryDate &amp;&amp; lecturerEntryDate.isBefore(tenYearsFromNow)&#xA;def level_four = between40and55&#xA;def level_five = moreThan55 &amp;&amp; lecturerEntryDate &amp;&amp; !lecturerEntryDate.isBefore(fiveYearsFromNow)&#xA;def level_six = moreThan55 &amp;&amp; between5and10&#xA;def level_seven = moreThan55 &amp;&amp; lecturerEntryDate &amp;&amp; lecturerEntryDate.isBefore(tenYearsFromNow)&#xA;def factorFerienDienstalter = 0&#xA;holyday = factorFeiertag * wageRate&#xA;if (level_one) {&#xA;    factorFerienDienstalter = 0.0833&#xA;}&#xA;else if (level_two) {&#xA;    factorFerienDienstalter = 0.0933&#xA;}&#xA;else if (level_three) {&#xA;    factorFerienDienstalter = 0.1041&#xA;}&#xA;else if (level_four) {&#xA;    factorFerienDienstalter = 0.1064&#xA;}&#xA;else if (level_five) {&#xA;    factorFerienDienstalter = 0.1064&#xA;}&#xA;else if (level_six) {&#xA;    factorFerienDienstalter = 0.1164&#xA;}&#xA;else if (level_seven) {&#xA;    factorFerienDienstalter = 0.1272&#xA;}&#xA;def factor = holyday + factorFerienDienstalter * wageRate + wageRate&#xA;factor = factor * education_amount_unit&#xA;factor&#xA;"/>
      <where>unique_id = 'ecap'</where>
    </update>
  </changeSet>
</databaseChangeLog>
```

## Documentation

A [rendered version for the docs](https://toccoag.gitlab.io/groovymarker) is available.

To browse rustdocs locally, the command `cargo doc --open` may be used to generate a documentation page and open it in a browser.

